package ro.pentalog.coursesix;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class CalendarActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar);
    }
    @Override
    public void onBackPressed(){
        Intent i = new Intent(getApplicationContext(),SettingActivity.class);
        startActivity(i);
    }
}
