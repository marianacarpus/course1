package ro.pentalog.coursesix;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View.*;
import android.view.View;
import android.widget.Button;

public class ProfileActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        Button btnMenu=(Button)findViewById(R.id.btnMenu);
                btnMenu.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(getApplicationContext(),MainActivity.class);
                        startActivity(i);

                    }
                });
                    Button btnSend=(Button)findViewById(R.id.btnSend);
                btnSend.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent emailIntent = new Intent(Intent.ACTION_SEND);
                            emailIntent.setType("text/plain");
                            emailIntent.putExtra(Intent.EXTRA_TEXT, "Test Mail");
                            startActivity(Intent.createChooser(emailIntent, "Send Email via"));
                            finish();
                        }
                    });
        Button btnSetting=(Button)findViewById(R.id.btnSetting);
        btnSetting.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(),SettingActivity.class);
                startActivity(i);

            }
        });

    }
    @Override
    public void onBackPressed(){
        super.onStop();
       Intent intent = new Intent(Intent.ACTION_MAIN);
       intent.addCategory(Intent.CATEGORY_HOME);
       intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);

}

    }

