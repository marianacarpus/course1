public class Ex2 {

   static void sumOfDigits(int a){
        int sum = 0;
        while ( a > 0 )
        {
            sum += a%10;
            a /= 10;
        }
        System.out.println(sum);
   }
    public static void main(String[] args) {
        sumOfDigits(235230);
    }
    
}
