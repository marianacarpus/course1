
package ex2.course3;


import java.util.*;


public class Ex2Course3 {
    public static void switchPairs(ArrayList<String> wordList){
        for(int i =0; i<wordList.size()-1; i+=2){
            Collections.swap(wordList, i, (i+1));
        }
  System.out.println(wordList);
}

    public static void main(String[] args) {
        ArrayList<String> wordList1 = new ArrayList<>(Arrays.asList("four", "score", "and", "seven", "years", "ago" ));
        ArrayList<String> wordList2 = new ArrayList<>(Arrays.asList("to", "be", "or", "not", "to", "be", "hamlet" ));
        switchPairs(wordList1);
        switchPairs(wordList2);
    }
    
}
