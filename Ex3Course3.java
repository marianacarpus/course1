
package ex3.course3;

import java.util.regex.*;

public class Ex3Course3 {

   public static void machTag(String str){
        boolean matchFound = false;
            Pattern r = Pattern.compile("<(.+)>([^<]+)</\\1>");
            Matcher m = r.matcher(str);

            while (m.find()) {
                System.out.println(m.group(2));
                matchFound = true;
            }
            if ( ! matchFound) {
                System.out.println("Incorrect tag");
            }
    }
   
 public static void main(String[] args){
       
         machTag("<h1>Hello World</h2>");
         machTag("<tag>contents</tag>");
    }
    
}
