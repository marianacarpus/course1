
public class Ex4 {

    public static boolean isPalindrom(String a)
    {
        int i = 0;
        int j = a.length() - 1;
        while( i != j )
        {
            if( a.charAt(i) != a.charAt(j) )
            {
                return false;
            }
            i++;
            j--;
        }
        return true;
    }
    
    public static void main(String[] args) {
        if(isPalindrom("abcdedcba"))
            System.out.println("Yes");
        else
            System.out.println("No");
    
    }
    
}
