
package ex4.course3;
import java.util.*;

public class Ex4Course3 {
    public static void markLength4(ArrayList<String> wordList) {

    for (int i = 0; i < wordList.size(); i++) {
        if (wordList.get(i).length() == 4) {
            wordList.add(i, "****");
            i++;
        }
    }
    System.out.println(wordList);
    
}
    public static void main(String[] args) {
        ArrayList<String> wordList = new ArrayList<>(Arrays.asList("this", "is", "lots", "of", "fun", "for", "every", "Java", "programmer" ));
        markLength4(wordList);
    }
    
}
