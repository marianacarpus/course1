
public class Ex6 {

    static void resolve(int n){
       int m = 1;
        for(int row = 0 ; row < n; row++)
        {
            for(int j = row; j < n - 1; j++ )
            {
                System.out.print("=");
            }
            System.out.print("/");
            for(int j = 0; j < m; j++)
            {
                System.out.print("_");
            }
            System.out.print("\\");
            for(int j = row; j < n - 1; j++ )
            {
                System.out.print("=");
            }
            System.out.println();
            
            m += 2;
        }
        
        
    }
    public static void main(String[] args) {
     resolve(5);
    }
    
}
