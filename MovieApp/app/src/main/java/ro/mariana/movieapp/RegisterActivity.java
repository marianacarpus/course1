package ro.mariana.movieapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.View.*;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegisterActivity extends AppCompatActivity {

    EditText editName;
    EditText editEmail;
    EditText editPassword;
    EditText editConfirmPass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        editName = (EditText) findViewById(R.id.editName);
        editEmail = (EditText) findViewById(R.id.editEmail);
        editPassword=(EditText)findViewById(R.id.editPassword);
        editConfirmPass=(EditText)findViewById(R.id.editPasswordConfirm);
        Button btnRegister = (Button)findViewById(R.id.btnRegister);
        btnRegister.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                final String name=editName.getText().toString();
                if(!isValidName(name)){
                    editName.setError("Invalid Name");
                }
                final String email = editEmail.getText().toString();
                if (!isValidEmail(email)) {
                    editEmail.setError("Invalid Email");
                }

                final String pass = editPassword.getText().toString();
                if (!isValidPassword(pass)) {
                    editPassword.setError("Invalid Password");
                }
                final String confPass=editConfirmPass.getText().toString();
                if(!isValidConfirmPassword(pass,confPass)){
                    editConfirmPass.setError("Password doesn't match");
                }
                if(isValidName(name) && isValidEmail(email) && isValidPassword(pass) && isValidConfirmPassword(pass,confPass)){

                    SharedPreferences myPrefs = getSharedPreferences("prefID", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = myPrefs.edit();
                    editor.putString("nameKey", editName.getText().toString());
                    editor.putString("emailKey", editEmail.getText().toString());
                    editor.putString("passwordKey", editPassword.getText().toString());
                    editor.apply();
                    TextView label=(TextView)findViewById(R.id.txtSucces);
                    label.setText("Succes Record!!");
                }

            }
        });

    }
    private boolean isValidName(String name) {
        if(name != null && name.length() >=3) {
            return true;
        }
        return false;
    }
    private boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    private boolean isValidPassword(String pass) {
        if(pass != null && pass.length() >=4) {
            return true;
        }
        return false;
    }
    private boolean isValidConfirmPassword(String pass,String confPass) {
        if(confPass != null && pass == confPass) {
            return true;
        }
        return false;
    }
}

